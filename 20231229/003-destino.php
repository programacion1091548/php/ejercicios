<?php
// si no ha cargado el formulario le obligo a cargarlo
if (!$_POST) {
    header("Location: 003-formularioTresBotones.php");
}
//var_dump($_POST);
//funcion que recibe un tecto y un caracter
//debe retornar el numero de veces que aparece el caracter
//y el texto debe modificar sustituyendo el caracter por el guion
function caracteres(string $texto, string $caracter): int{
$resultado = 0;
    for( $i = 0; $i < strlen($texto); $i++){
        if($texto[$i] == $caracter){
            $texto[$i] = "-";
            $resultado++;
        }
    }
    return $resultado;
}

