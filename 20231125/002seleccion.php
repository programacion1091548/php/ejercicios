<?php
//si el numero de unidades perdas esta entre
//0-10; descuento 0
//11-20; descuento 10
//21-30; descuento 20
// mayor o igual que 31 : descuento 30

$numero = 25;
$descuento = 0;

//realizar el ejercicio utilizando if

if ($numero >= 0 && $numero <= 10) {
    $descuento = 0;
} elseif ($numero >= 11 && $numero <= 20) {
    $descuento = 10;

} elseif ($numero >= 21 && $numero <= 30) {
    $descuento = 20;

} else {
    $descuento = 30;

}