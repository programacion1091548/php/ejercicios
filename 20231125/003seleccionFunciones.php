<?php

/**
 * 
 * Calcular el descuento de un numero de unidades pedido
 * si el numero de unidades pedidas esta entre
 * 0-10 : descuento 0
 * 11-20 : descuento 10
 * 21-30 : descuento 20
 * mayor o igual que 31 : descuento 30
 * @param int $numero
 * @return int descuento calculado
 */
function calcularDescuento(int $numero): int
{
    $descuento = 0;
    if ($numero <= 10) {
        $descuento = 0;
    } elseif ($numero <= 20) {
        $descuento = 10;
    } elseif ($numero <= 30) {
        $descuento = 20;
    } else {
        $descuento = 30;
    }
    return $descuento;
}


function calcularColor($inicial)
{
}


$solucion = calcularDescuento(20);
echo $solucion;

$color = calcularColor("r");
echo $color;

