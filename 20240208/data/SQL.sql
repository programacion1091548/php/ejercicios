﻿-- Lenguaje de definicion de datos

DROP DATABASE IF EXISTS personas;
CREATE DATABASE personas;
USE personas;

CREATE TABLE persona(
  idPersona int AUTO_INCREMENT,
  nombre varchar(200),
  edad int,
  PRIMARY KEY(idPersona)
); 

-- Lenguaje de manipulacion de datos

INSERT INTO persona 
  (nombre, edad) VALUES 
  ('persona1', 18),
  ('persona2',45);

SELECT p.idPersona,
       p.nombre,
       p.edad FROM persona p;


-- creo la tabla cliente
-- nombre
-- edad
-- nombreEmpresa
-- telefono

CREATE TABLE cliente(
  idCliente int AUTO_INCREMENT,
  nombre varchar(200),
  edad int,
  nombreEmpresa varchar(200),
  telefono varchar(20),
  PRIMARY KEY(idCliente)
);

-- meto dos registros en la tabla cliente

INSERT INTO cliente 
(nombre, edad, nombreEmpresa, telefono) VALUES 
('Luis', 50, 'Alpe', '942942942'),
('Ana',20,'Alpe','456456456');