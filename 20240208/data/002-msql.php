<?php

// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla persona
// mostrar los registros de la tabla persona
// cerrar conexión

$conexion= new mysqli(
    'localhost',
    'root',
    '',
    'personas'

);

$resultados = $conexion->query("SELECT c.idCliente, c.nombreCliente, c.edadCliente, c.nombreEmpresa, c.telefono FROM  cliente c");

$registros= $resultados->fetch_all(MYSQLI_ASSOC);

foreach ($registros as $indice => $registro) {
    ?>
    <h2>Registro <?= $indice ?> </h2>
    <ul>
        <li>idCliente: <?= $registro['idCliente'] ?></li>
        <li>nombreCliente: <?= $registro['nombreCliente'] ?></li>
        <li>edadCliente: <?= $registro['edadCliente'] ?></li>
        <li>nombreEmpresa: <?= $registro['nombreEmpresa'] ?></li>
        <li>telefono: <?= $registro['telefono'] ?></li>
    </ul>
    <hr>
    <?php
}
$conexion->close();

