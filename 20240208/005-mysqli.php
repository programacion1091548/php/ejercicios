<?php
// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla persona
// utilizamos el metodo FETCH_OBJECT
// mostrar los registros de la tabla persona
// cerrar conexión

// conectarse al servidor
$conexion = new mysqli("localhost", "root", "", "personas");

// realizo la consulta
$resultados = $conexion->query("SELECT * FROM persona");

// para mostrar los resultados 
// voy a utilizar fecht_object
// fetch_object te crea un objeto de la clase anomina
// esa clase se denomina stdClass
// new stdClass() ==> crea un objeto vacio 
// mediante un while

$contador = 0;
while ($registro = $resultados->fetch_object()) {
    echo "<h2>Registro numero {$contador}</h2>";
    echo "<ul>";
    echo "<li>IdPersona : {$registro->idPersona}</li>";
    echo "<li>Nombre : {$registro->nombre}</li>";
    echo "<li>Edad : {$registro->edad}</li>";
    echo "</ul>";
    $contador++;
}

// reiniciar el puntero
$resultados->data_seek(0);

// mediante un while
// un foreach
$contador = 0;
while ($registro = $resultados->fetch_object()) {
    echo "<h2>Registro numero {$contador}</h2>";
    echo "<ul>";
    foreach ($registro as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo} : {$valorCampo}</li>";
    }
    echo "</ul>";
    $contador++;
}


// cerrar la conexión
$conexion->close();
