<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $resultado = 0;
    $numero1 = 0;
    $numero2 = 0;
    // quiero que cuando pulse el boton de enviar
    // me sume los dos numeros y me colocoque el resultado
    // si no pulsa el boton de enviar debe cargar el formulario
    // en caso de que los numeros esten vacios se supone que 
    // su valor es cero
    if ($_GET) {
        $numero1 = $_GET["numero1"] ?: 0;
        // $numero1 = (int) $_GET["numero1"];
        $numero2 = $_GET["numero2"] ?: 0;
        // $numero2 = (int) $_GET["numero2"];
        $resultado = $numero1 + $numero2;
        echo $resultado;
    } else {
        require "_formulario2.php";
    }

    ?>
</body>

</html>