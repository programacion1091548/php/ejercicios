<?php
// inicializar las variables
$numero1 = 0;
$numero2 = 0;
$salida = "";

// introducir datos

$numero1 = 120;
$numero2 = 23;

// comprobar si el numero1 es mayor que el numero2

if ($numero1 > $numero2) {
    $salida = "el numero1 {$numero1} es mayor que el numero2 {$numero2}";
}

// muestro salida
?>

<div>
    <?= $salida ?>
</div>

<div>
    <?php printf("el numero1 %d es mayor que el numero %d", $numero1, $numero2); ?>
</div>