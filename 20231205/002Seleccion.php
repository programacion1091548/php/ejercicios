<?php
// inicializar las variables
$numero1 = 0;
$numero2 = 0;
$salida = "";

// introducir datos

$numero1 = 120;
$numero2 = 23;

if ($numero1 > $numero2) {
    $salida = "El numero1 {$numero1} es mayor que el numero2 {$numero2}";
} else {
    $salida = "El numero2 {$numero2} es mayor que el numero1 {$numero1}";
}

?>

<div>
    <?= $salida ?>
</div>