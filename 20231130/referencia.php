<?php

// asignar valor
$a = 10;

$b = $a; // a la variable b solo le paso el valor de a

echo $b; // 10
echo $a; // 10

$a = 34;

echo $b; // 10
echo $a; // 34

// asignar referencia
$b = &$a; // a la variable b le paso la referencia de a

echo $b; // 34
echo $a; // 34

$a = 89;

echo $b; // 89
echo $a; // 89

$b = 100;

echo $b; // 100
echo $a; // 100
