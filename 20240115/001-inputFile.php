<?php
// el post está vacio 
// var_dump($_POST);

// el files contiene los metadatos de los ficheros
// var_dump($_FILES);

// controlar si he pulsado el boton
$mensaje = "";

if ($_FILES) {

    // creo un array con todos los metadatos del archivo
    $miFichero = $_FILES['fichero'];

    // ruta donde guardo los archivos subidos dentro de mi servidor web
    $rutaDestino = "./ficheros/" . $miFichero['name'];

    // para mover el archivo de la carpeta temporal
    // donde lo coloca el servidor a la ruta destino
    $ficheroSubido = move_uploaded_file($miFichero['tmp_name'], $rutaDestino);
    if ($ficheroSubido) {
        $mensaje = "fichero subido";
    } else {
        $mensaje = "fichero no subido";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="fichero">
        <br>
        <button>Enviar</button>
    </form>
    <?= $mensaje ?>
</body>

</html>