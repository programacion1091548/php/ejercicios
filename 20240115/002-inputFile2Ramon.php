<?php
// el post esta vacio
var_dump($_POST);

// el files contiene los metadatos de los ficheros subidos
var_dump($_FILES);

// inicializo las variables

// con variables
$mensajePdf = "";
$mensajeImagen = "";

// compruebo si he pulsado el boton de enviar
if ($_FILES) {
    // creo un array con todos los metadatos del fichero

    // si queremos dos variables
    $pdf = $_FILES["pdf"];
    $imagen = $_FILES["imagen"];


    // ruta donde guardo los archivos subidos dentro del servidor
    $rutaDestinoPdf = "./pdf/" . $pdf["name"];
    $rutaDestinoImagen = "./imgs/" . $imagen["name"];

    // para mover el archivo de la carpeta temporal
    // donde lo coloca el servidor a la ruta destino
    $ficheroSubidoPdf = move_uploaded_file($pdf["tmp_name"], $rutaDestinoPdf);
    $ficheroSubidoImagen = move_uploaded_file($imagen["tmp_name"], $rutaDestinoImagen);


    // utilizando variables
    if ($ficheroSubidoPdf) {
        $mensajePdf = "Se ha subido el fichero PDF";
    } else {
        $mensajePdf = "No se ha podido subir el fichero PDF";
    }

    if ($ficheroSubidoImagen) {
        $mensajeImagen = "Se ha subido la imagen";
    } else {
        $mensajeImagen = "No se ha podido subir la imagen";
    }
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="pdf">
        <br>
        <input type="file" name="imagen">
        <br>
        <button>Enviar</button>
    </form>

    <div>
        <?= $mensajePdf ?>
    </div>
    <div>
        <?= $mensajeImagen ?>
    </div>


</body>

</html>
