<?php
// el post está vacio 
// var_dump($_POST);

// el files contiene los metadatos de los ficheros
// var_dump($_FILES);

// controlar si he pulsado el boton
$mensaje = "";

if ($_FILES) {

    // creo un array con todos los metadatos del archivo
    $miFicheroImg = $_FILES['fichero'];
    $miFicheroPdf = $_FILES['fichero1'];

    // ruta donde guardo los archivos subidos dentro de mi servidor web
    $rutaDestinoImg = "./imgs/" . $miFicheroImg['name'];
    $rutaDestinoPdf= "./pdf/" . $miFicheroPdf['name'];

    // para mover el archivo de la carpeta temporal
    // donde lo coloca el servidor a la ruta destino
    $ficheroSubidoImg = move_uploaded_file($miFicheroImg['tmp_name'], $rutaDestinoImg);
    $ficheroSubidoPdf = move_uploaded_file($miFicheroPdf['tmp_name'], $rutaDestinoPdf);
    if ($ficheroSubidoImg && $ficheroSubidoPdf) {
        $mensaje = "ficheros subidos";
    } else {
        $mensaje = "ficheros no subidos";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post" enctype="multipart/form-data">
        <div>
            <label for="fichero">Imagen</label>
        <input type="file" name="fichero">
        </div>
        <div>
            <label for="fichero1">PDF</label>
        <input type="file" name="fichero1">
        </div>
        <br>
        <button>Enviar</button>
    </form>
    <?= $mensaje ?>
</body>

</html>