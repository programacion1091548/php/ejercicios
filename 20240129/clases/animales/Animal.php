<?php
namespace clases\animales;
class Animal
{
    public string $nombre;    
    public float $peso;
    public string $color;

    public function __construct(string $nombre="", float $peso=0, string $color="")
    {
        $this->nombre = $nombre;
        $this->peso = $peso;
        $this->color = $color;
    }
}
