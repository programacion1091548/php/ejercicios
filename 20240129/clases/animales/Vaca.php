<?php
namespace clases\animales;

class Vaca
{
    public string $nombre;
    public float $peso;
    public string $color;
    public string $direccion;
    public string $granja;

    public function __construct(string $nombre="", float $peso=0, string $color="", string $direccion="", string $granja="")
    {
        $this->nombre = $nombre;
        $this->peso = $peso;
        $this->color = $color;
        $this->direccion = $direccion;
        $this->granja = $granja;
    }
}