<?php
namespace clases\herencia;
class Animal
{
    public string $nombre;    
    public float $peso;
    public string $color;

    public function __construct(string $nombre="animal", float $peso=0, string $color="sin color")
    {
        $this->nombre = strtoupper($nombre); // convierte a mayúsculas
        $this->peso = $peso;
        $this->color = $color;
    }
}
