<?php
// inicializo la sesion
session_start();
// quiero un formulario que me permita introducir un numero
// y quiero que me muestre todos los numeros introducidos
// quiero realizar el ejercicio con sesiones
// inicializo la variable
$numero = [];
// he pulsado el boton de enviar
if ($_POST) {
    if (isset($_SESSION['numero'])) {
        $numero = $_SESSION['numero'];
    } else {
        $numero = [];
    }
    array_push($numero, $_POST["numero"]);
    $_SESSION["numero"] = $numero;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" id="numero" name="numero">
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
    <?php
    var_dump($numero);
    ?>
</body>
</html>