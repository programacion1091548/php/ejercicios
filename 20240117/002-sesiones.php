<?php
//mostrar el ultimo numero introducido en el formulario del ejercicio 1
//el numero almacenado en la variable de la sesión
session_start();
$numero=0;
if (isset($_SESSION['numero'])) {
    $numero = $_SESSION['numero'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>Ultimo numero introducido: <?= $numero ?></p>   
</body>
</html>