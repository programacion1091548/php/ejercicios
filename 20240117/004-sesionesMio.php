<?php
session_start();
$mensaje = "";
$reinicio = 0;



//se trata de realizar un pequeño juego donde
//el usuario tiene que adivinar los 10 numeros del array
//creamos un formulario donde escribo un numero y cuando pulso
//enviar debe comprobar si el numero esta en el array
//si esta en el array me indica que he acertado y me lo muetra
//si fallo me cuenta un fallo y me debe mostrar el numero de fallos
//cuando adivine los 10 numeros la puntuacion sera el numero de intentos
//para adivinar los 10 numeros

//una vez que adivina los 10 numeros debe borrar las variables de session

//creo la variable numeros
//leyendo los valores de la variable de la sesion numeros
//la variable de sesion numeros tendrá los numeros que me trendra los numeros que me quedan por acertar
//si la variable de sesion numerosno existe introduzco
//en la variable numeros los 10 numeros


if (!isset($_SESSION['aciertos'])) {
    $_SESSION['numeros'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    $_SESSION['aciertos'] = [];
    $_SESSION['fallos'] = [];
    $_SESSION['intentos'] = 0;
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Juego de Adivinanza</title>
</head>

<body>

    <h1>Juego de Adivinanza</h1>

    <form method="post" action="">
        <label for="numero">Escribe un número:</label>
        <input type="number" name="numero" required>
        <button type="submit" name="jugar">Jugar</button>
        <button type="reset" name="reiniciar">Reiniciar</button>
    </form>
    <?php
    // if ($reinicio = 1) {
    //     
    // } else {
    if ($_POST) {
        if (
            in_array($_POST['numero'], $_SESSION['numeros'])
            &&
            !in_array($_POST['numero'], $_SESSION['aciertos'])
        ) {
            $_SESSION['aciertos'][] = $_POST['numero'];
            echo "¡Has acertado el número " . $_POST['numero'] . "!<br>";
        } else {


            $_SESSION['fallos'][] = $_POST['numero'];
            echo "Fallo. Número de intentos fallidos: " . count($_SESSION['fallos']) . "<br>";
        }

        $_SESSION['intentos']++;
        echo "Intentos: " . $_SESSION['intentos'] . "<br>";


        if (count($_SESSION['numeros']) == count($_SESSION['aciertos'])) {
            echo "¡Felicidades! Has adivinado todos los números en " . $_SESSION['intentos'] . " intentos.<br>";
            ?>
            <img src="unicornio.gif" alt="">
            <audio src="smb_stage_clear.wav" autoplay></audio>
            <?php
            unset($_SESSION['numeros']);
            unset($_SESSION['intentos']);
            unset($_SESSION['fallos']);
            session_destroy();

        }

    }
?>



</body>

</html>