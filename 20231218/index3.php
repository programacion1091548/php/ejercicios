<?php
// cargar la libreria 003-arrays.php
require_once '003-arrays.php';

// cargar los datos que estan en bbdd.php
require_once 'bbdd.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <h1>Montrar Array bidimensional</h1>
    <?= mostrarDatos($alumnos) ?>
    <h1>Montrar Array bidimensional</h1>
    <?= mostrarDatosv1($alumnos) ?>
    <h1>Montrar Array bidimensional</h1>
    <?= mostrarDatosv2($alumnos) ?>
</body>
