<?php
require_once "002-arrays.php";

require "datos.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <h2>Recorrer array con for</h2>
    <?= mostrarArray($ciudades) ?>
    <?= mostrarArray($edades) ?>
    <h2>Recorrer array con for ayudandonos de array_values y array_key </h2>
    <?= mostrarArrayV1($ciudades) ?>
    <?= mostrarArrayV1($edades) ?>
    <?= mostrarArrayV1($valores) ?>
    <?= mostrarArrayV1($colores) ?>
    <h2>Recorrer array con foreach </h2>
    <?= mostrarArray2($ciudades) ?>
    <?= mostrarArray2($edades) ?>
    <?= mostrarArray2($valores) ?>
    <?= mostrarArray2($colores) ?>
    <h2>Recorrer array con array_walk </h2>
    <?= mostrarArray3($ciudades) ?>
    <?= mostrarArray3($edades) ?>
    <?= mostrarArray3($valores) ?>
    <?= mostrarArray3($colores) ?>
    <h2>Recorrer array con array_walk e implode </h2>
    <?= mostrarArray3V1($ciudades) ?>
    <?= mostrarArray3V1($edades) ?>
    <?= mostrarArray3V1($valores) ?>
    <?= mostrarArray3V1($colores) ?>

    <h2>Recorrer array con array_map e implode </h2>
    <?= mostrarArray4($ciudades) ?>
    <?= mostrarArray4($edades) ?>
    <?= mostrarArray4($valores) ?>
    <?= mostrarArray4($colores) ?>
</body>

</html>