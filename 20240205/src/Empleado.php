<?php

namespace src;

class Empleado extends Persona
{
    private ?float $sueldo;

    // sobreescribir el metodo mostrar
    public function mostrar(): string
    {
        $salida = "<ul>";
        $salida .= "<li>Nombre:" . $this->getNombre() . "</li>";
        $salida .= "<li>Edad:" . $this->getEdad() . "</li>";
        $salida .= "<li>Sueldo:" . $this->sueldo . "</li>";
        $salida .= "</ul>";
        return $salida;
    }

    public function __construct()
    {
        parent::__construct();
        $this->sueldo = null;
    }
    public function calcularSueldo(int $horas): void
    {
        $this->sueldo = $horas * 10;
    }

    /**
     * Get the value of sueldo
     *
     * @return ?float
     */
    public function getSueldo(): ?float
    {
        return $this->sueldo;
    }
}
