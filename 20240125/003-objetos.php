<?php
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});
$objeto = new \clases\erica\Numeros([1, 1, 2, 2, 3]); // este no funciona correctamente devuelve 1
var_dump($objeto);
echo $objeto->calcularModa();
$objeto1 = new clases\profesor\Numeros([1, 1, 2, 2, 3]); // este funciona correctamente devuelve 1,5
var_dump($objeto1);
echo $objeto1->calcularModa();