<?php
// Voy a utilizar los espacios de nombres
// para poder tener la misma clase en varias carpetas
// para poder tener organizadas mis clases

// coloco la autocarga de clases
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$objeto1 = new clases\profesor\Numeros([2, 3, 5, 2, 2]);

var_dump($objeto1);

// echo $objeto1->calcularMedia(); // esta clase no tiene ese metodo

$objeto2 = new clases\erica\Numeros([1, 2, 3, 2, 2]);

var_dump($objeto2);

echo $objeto2->calcularMedia();
echo $objeto2->calcularModa();
echo $objeto2->calcularMediana();

