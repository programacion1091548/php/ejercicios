<?php

// importo la clase al espacio de nombres actual

use clases\profesor\Texto;
use clases\erica\Texto as EricaTexto;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

// crear objeto de tipo texto

$objeto = new Texto();
$onjeto1 = new EricaTexto();

echo $objeto->mostrar();
