<?php
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';});

    //creo un objeto de tipo coche
    $ford1 = new Coche();

    //creo un objeto de tipo coche
    $ford2 = new Coche("rojo", 5, "ford", 100);

$ford1-> setMarca("ford");
// $ford2->gasolina(10); -> esto da error pq ya tenemos el set y el get y está private
$ford2->setGasolina(10);

echo $ford1->getGasolina();
$ford2->llenarTanque(10);


    var_dump($ford2);
    var_dump($ford1);