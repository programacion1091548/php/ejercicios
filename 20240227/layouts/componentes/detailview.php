<div class="card" style="width: 30rem;">
    <div class="card-header">
        <?= $titulo ?>
    </div>
    <ul class="list-group list-group-flush">
        <?= $contenido ?>
    </ul>
</div>