<?php
header("Content-type: image/png");
$ancho = 200;
$alto = 100;
// Creo el lienzo
$imagen = imagecreatetruecolor($ancho, $alto);

// Defino colores
$colorFondo = imagecolorallocate($imagen, 177, 177, 177);
$negro = imagecolorallocate($imagen, 0, 0, 0);
$rojo = imagecolorallocate($imagen, 255, 0, 0);
$verde = imagecolorallocate($imagen, 0, 255, 0);
$azul = imagecolorallocate($imagen, 0, 0, 255);

// Relleno el fondo
imagefill($imagen, 0, 0, $colorFondo);

// Dibujo un rectángulo
imagerectangle($imagen, 50, 25, 150, 75, $negro);
imagefilledrectangle($imagen, 51, 26, 149, 74, $azul);

// Envío la imagen al navegador
imagepng($imagen);