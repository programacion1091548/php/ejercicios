<?php
header("Content-type: image/png");
$ancho = 200;
$alto = 200;
// Creo el lienzo
$imagen = imagecreatetruecolor($ancho, $alto);

// Defino colores
$colorFondo = imagecolorallocate($imagen, 177, 177, 177);
$negro = imagecolorallocate($imagen, 0, 0, 0);
$colorTriangulo = imagecolorallocate($imagen, 0, 0, 255);

// Relleno el fondo
imagefill($imagen, 0, 0, $colorFondo);

// Genero puntos aleatorios para los vértices del triángulo
$punto1_x = mt_rand(0, $ancho);
$punto1_y = mt_rand(0, $alto / 2); // Limito la altura para asegurar que el triángulo sea visible
$punto2_x = mt_rand(0, $ancho);
$punto2_y = mt_rand($alto / 2, $alto);
$punto3_x = mt_rand(0, $ancho);
$punto3_y = mt_rand($alto / 2, $alto);


// Defino los puntos del triángulo
$puntos = [
    $punto1_x, $punto1_y,  // Punto 1
    $punto2_x, $punto2_y,  // Punto 2
    $punto3_x, $punto3_y   // Punto 3
];

// Dibujo el triángulo
imagefilledpolygon($imagen, $puntos,$colorTriangulo);

// Envío la imagen al navegador
imagepng($imagen);

?>