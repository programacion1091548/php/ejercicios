<?php
header("Content-type: image/png");
// radio del circulo que voy a dibujar
$radio = mt_rand(10, 100);

// dos variables que definen el alto y el ancho del lienzo de dibujo
$width = 200;
$height = 200;

// creo lienzo dibujo
$imagen = imagecreatetruecolor($width, $height);

// coloco color fondo
$colorFondo = imagecolorallocatealpha($imagen, 177, 177, 177, 50);
// relleno el lienzo con el color de fondo
imagefill($imagen, 0, 0, $colorFondo);
// creo colores
$negro = imagecolorallocate($imagen, 0, 0, 0);
$rojo = imagecolorallocate($imagen, 255, 0, 0);
$verde = imagecolorallocate($imagen, 0, 255, 0);
$color1 = imagecolorallocatealpha($imagen, 0xC, 0xC, 0xC, 100);


//dibujo una linea
// imageline($imagen, 10, 10, 100, 10, $rojo);
// imageline($imagen, 100, 10, 10, 100, $rojo);
// imageline($imagen, 10, 100, 10, 10, $rojo);

// dibuja la imagen
imagepng($imagen);
