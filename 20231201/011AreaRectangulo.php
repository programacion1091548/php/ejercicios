<?php
// funcion que recibe como argumento el lado de un rectangulo y 
// devuelve el area de ese rectangulo
function areaRectangulo(int $lado): float
{
    return $lado ** 2;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Calcular el area de un circulo de area aleatoria</h1>
    <?php
    // leer el lado que viene del formulario
    $lado = $_GET["lado"];

    // dibujo el rectangulo
    // mando el lado por get en la url

    echo "<img src='009DibujarGD.php?lado={$lado}'>";

    // calculo el area 
    $area = areaRectangulo($lado);

    // mostrar el radio y el area calculada
    ?>
    <ul>
        <li>lado: <?= $lado ?></li>
        <li>Area: <?= $area ?></li>
    </ul>


</body>

</html>