<?php
// Crear un formulario que me permita introducir un numero
// almacenar ese numero en una cookie y mostrarlo por pantalla
// muestro el numero introducido por pantalla y el anterior

// Si he pulsado el boton enviar, guardo el numero en la cookie
if ($_POST) {

    $nombreCookie = "numeroC";
    $valorCookie = $_POST["numero"];
    $tiempoCaducidad = time() + 3600; // Caduca en 1 hora (3600 segundos)
// almaceno el número introducido en una cookie llamada numeroC durante 1 hora
    setcookie($nombreCookie, $valorCookie, $tiempoCaducidad);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="number">Numero</label>
            <input type="number" name="numero" id="numero" title="introduce numero" placeholder="Introduce un numero"
                required>
        </div>
        <button>Enviar</button>
    </form>

    <br>

    <h1>DATOS INTRODUCIDOS</h1>
    <div class="etiqueta">
        <span class="etiqueta">Numero introducido actualmente</span> :
        <?= $_POST["numero"] ?? "" ?>
    </div>
    <div class="etiqueta">
        <span class="etiqueta">Numero introducido anteriormente</span> :
        <?= $_COOKIE["numeroC"] ?? "" ?>
    </div>

</body>

</html>