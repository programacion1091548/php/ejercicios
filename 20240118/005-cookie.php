<?php

$numeros = [1, 2, 3, 4, 5];

// para meter un array en una cookie hay que serializar el array en un string 
// para mostrarlo hay que deserializarlo

setcookie("numeros", serialize($numeros), time() + 3600);

var_dump($_COOKIE);

var_dump(unserialize($_COOKIE["numeros"]));
