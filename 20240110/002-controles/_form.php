 <h1>Usar cuadros de lista</h1>
 <form method="post">
     <div>
         <label for="frutas">Elija sus frutas preferidas</label>
         <br>
         <select name="frutas[]" id="frutas" multiple>
             <option>Manzana</option>
             <option>Naranja</option>
             <option>Pera</option>
             <option>Pomelo</option>
         </select>
     </div>
     <br>
     <div>
         <button>Enviar</button>
     </div>
 </form>