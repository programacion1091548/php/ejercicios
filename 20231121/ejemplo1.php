<?php
/*
crear variables inicializandolas
*/

// creo una variable y la inicializo a 1
$numero = 1;

$texto = "hola";//creando una variable de tipo string

/*
fin de la creación de variables
*/
//$numero=1
$numero = $numero + 1;
//$numero=2
$numero++; // esto lo que hace es sumar el valor de la variable +1, post incremento
//$numero=3
++$numero;
//$numero=4 

$final = $numero++; // post incremento 
$numero = 5;
$final = 4;

$final = ++$numero; // pre incremento
$numero = 4;
$final = 5;

$final = ($numero++); //primero hace lo del paréntesis y luego lo asigna a final)
$numero = 5;
$final = 5;


$numero += 1; // operador contraído, $numero = $numero +2;
//$numero = 6

echo "la variable número vale ". $numero . "<br>";
echo "La variable número vale $numero <br>";
echo 'La variable número vale $numero <br>';
echo "La variable número vale {$numero} <br>"; // mejor escribir esta forma que la q está en la línea 38





