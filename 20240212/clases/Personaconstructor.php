<?php


namespace clases;

class Persona
{
    public int $id;
    public string $nombre;
    public string $apellidos;
    public int $edad;
    public string $fechaNacimiento;

    public string $poblacion;

    public string $codigoPostal;

    public function __toString()
    {
        $salida = "<ul>";
        foreach ($this as $campo => $valor) {
            $salida .= "<li>{$campo} : {$valor}</li>";
        }
        $salida .= "</ul>";
        return $salida;
    }
    public function __construct($id=0   , $nombre=  "", $apellidos="", $edad=0, $fechaNacimiento="", $poblacion="", $codigoPostal=0)
        {
            $this->id = $id;
            $this->nombre = $nombre;
            $this->apellidos = $apellidos;
            $this->edad = $edad;
            $this->fechaNacimiento = $fechaNacimiento;
            $this->poblacion = $poblacion;
            $this->codigoPostal = $codigoPostal;
        }
        }