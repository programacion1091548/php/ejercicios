<?php

// CONTROLAR QUE HE PULSADO EL BOTON ENVIAR
if ($_POST) {
    // leyendo los datos
    $nombre = $_POST["nombre"] ?: "";
    $apellidos = implode(" ", $_POST["apellidos"]); //implode separa los elementos de un array en strings
    $estado = $_POST["estado"] ?: "";
    $aficiones = implode(",", $_POST["aficiones"]);
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
 <form method="post">
     <div>
        <h1>Datos Personales</h1>
        <label for="nombre ">Nombre</label>
        <input type="text" id="nombre" name="nombre" >
    </div><br>
    <div>
        <label for="apellido1">Apellido1</label>
        <input type="text" name="apellidos[]" id="apellido1">
  
    
        <label for="apellido2">Apellido2</label>
        <input type="text" name="apellidos[]" id="apellido2">
    </div>
    <div>
        <h1>Estado Civil</h1>
        <label for="estado">Casado</label>
        <input type="radio" id="casada" name="estado" value="casada">
        <label for="estado">Soltero</label>
        <input type="radio" id="soltera " name="estado" value="soltera">
    
    </div>
    <div>
        <h1>Aficiones</h1>
    <input type="checkbox" id="aficiones" name="aficiones[]" value="deportes">
        <label for="aficiones">Deportes</label>
        <input type="checkbox" id="aficiones" name="aficiones[]" value="lectura">
        <label for="aficiones">Lectura</label>
        <input type="checkbox" id="aficiones" name="aficiones[]" value="dormir">
        <label for="aficiones">Dormir</label>
       
    </div><br>
    <div>
        <button type="submit">Enviar</button>
        <button type="reset">Borrar</button>
    </div>
     </form>
     <?php
     if ($_POST) {

     ?>
     <div>
     <h2>Datos</h2>
     <p>El nombre es <?= $nombre ?></p>
     <p>Los apellidos son <?= $apellidos ?></p>
     <p>El estado civil es <?= $estado ?></p>
     <p>Las aficiones son <?= $aficiones ?></p>
     </div>
     <?php
     }
     ?>
     </body>
</html>