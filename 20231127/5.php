<?php

$dias = [
    "lunes",
    "martes",
    "miercoles",
    "jueves",
    "viernes"
];

// esto no se puede realizar
// echo $dias;

// esto si pero solo para pruebas
var_dump($dias);

// quiero acceder al segundo dia
// leer
echo $dias[2];

// escribir 
$dias[2] = "miércoles";

var_dump($dias);
