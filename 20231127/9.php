<?php
// variables a utilizar
// variables con los datos introducidos
$a = 10;
$b = 3;
// variable para almacenar los resultados
$resultados = [];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operadores</title>
</head>

<body>
    <?php
    // Quiero que coloquemos los resultados de las siguientes operaciones
    // a+b
    // a-b
    // a*b
    // a resto b
    // a elevado b
    // a dividido entre b
    // a es igual a b
    // a menor o igual que b

    // colocamos una tabla en donde la primera columa es el nombre
    // de la operacion
    // en la segunda columna el resultado

    // procesamiento

    // inicializar el array
    // $resultados = [
    //     $a + $b,
    //     $a - $b,
    //     $a * $b,
    //     $a % $b,
    //     $a ** $b,
    //     $a / $b,
    // ];

    // añadir elementos al array
    // push
    $resultados[0] = $a + $b;
    $resultados[] = $a - $b;
    $resultados[] = $a * $b;
    $resultados[] = $a % $b;
    $resultados[] = $a ** $b;
    $resultados[] = $a / $b;


    if ($a == $b) {
        $resultados[] = "Si, son iguales";
    } else {
        $resultados[] = "No son iguales";
    }

    if ($a <= $b) {
        $resultados[] = "a es menor o igual que b";
    } else {
        $resultados[] = "a no es menor o igual que b";
    }

    // mostrar resultados
    // var_dump($resultados);
    ?>
    <table border="1">
        <tr>
            <td>Operaciones</td>
            <td>Resultados</td>
        </tr>
        <tr>
            <td>Suma</td>
            <td><?php echo $resultados[0] ?></td>
        </tr>
        <tr>
            <td>Restar</td>
            <td><?= $resultados[1] ?></td>
        </tr>
        <tr>
            <td>Producto</td>
            <td><?= $resultados[2] ?></td>
        </tr>
        <tr>
            <td>Resto</td>
            <td><?= $resultados[3] ?></td>
        </tr>
        <tr>
            <td>Elevado</td>
            <td><?= $resultados[4] ?></td>
        </tr>
        <tr>
            <td>Cociente</td>
            <td><?= $resultados[5] ?></td>
        </tr>
        <tr>
            <td>A igual a B</td>
            <td>
                <?= $resultados[6] ?>
            </td>
        </tr>
        <tr>
            <td>A menor o igual que B</td>
            <td>
                <?= $resultados[7] ?>
            </td>
        </tr>
    </table>
</body>

</html>