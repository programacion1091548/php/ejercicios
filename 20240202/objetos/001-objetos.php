<?php
class Padre
{
    public string $nombre;
    public string $apellidos;
    private string $tipo;
    public int $edad;
    protected int $altura;

    public int $nivel;


    public function __construct(string $nombre, string $apellidos, int $edad, int $altura, string $tipo)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->tipo = $tipo;
        $this->edad = $edad;
        $this->altura = $altura;
        $this->nivel = 1;
    }

    public function presentarme()
    {
        return "Soy el padre";
    }
    public function asignar($nombre, $apellidos)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
    }

    public function getEdad()
    {
        return $this->edad;
    }

    public function getTipo()
    {
        return $this->tipo;;
    }
}

class Hijo extends Padrehttps://www.google.es/maps/place/Holiday+Inn+London+-+Kensington+High+St.,+an+IHG+Hotel/@51.5017105,-0.1886071,3a,75y,259.98h,81.55t/data=!3m7!1e1!3m5!1saiSfGXfN7wZRjh6QEsQFbg!2e0!5s20120901T000000!7i13312!8i6656!4m19!1m8!3m7!1s0x48761b1c5f1a9429:0x5ffdbed0f63cba58!2sCamden+Town,+Londres,+Reino+Unido!3b1!8m2!3d51.5390261!4d-0.1425516!16zL20vMG5xcDM!3m9!1s0x48760ff6bb032e33:0x2dcbae0bd0f98b7d!5m2!4m1!1i2!8m2!3d51.498807!4d-0.1919659!10e5!16s%2Fg%2F1td9vlsh?entry=ttu#
{
    // la propiedad nivel esta sobreescrita
    public int $nivel; // no sirve para nada

    // la propiedad tipo que es privada la sobreescribo    
    public string $tipo;

    // sobreescritura constructor
    public function __construct(string $nombre, string $apellidos, int $edad, int $altura)
    {
        parent::__construct($nombre, $apellidos, $edad, $altura, "padre");
        $this->tipo = "hijo";
    }

    // sobreescribo el metodo getTipo
    public function getTipo()
    {
        return $this->tipo;
    }

    // sobreescribo el metodo presentarme
    public function presentarme()
    {
        return "Soy el hijo";
    }
}

$padre1 = new Padre("Pepe", "Perez", 18, 180, "padre");

$hijo1 = new Hijo("Luisa", "Perez", 5, 45);

var_dump($padre1);
var_dump($hijo1);

echo $padre1->getEdad();
echo "<br>";
echo $hijo1->getEdad();

echo $hijo1->presentarme();
