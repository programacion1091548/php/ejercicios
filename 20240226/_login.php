<div class="my-5 container">
    <div class="row justify-content-center">
        <div class="border bg-light rounded p-3 col-6">
            <form method="post">
                <div class="mb-3 row">
                    <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                </div>
                <div class="mb-3 row justify-content-center">
                    <button class="w-25 btn btn-primary">Ingresar</button>
                </div>
            </form>
        </div>
    </div>
</div>