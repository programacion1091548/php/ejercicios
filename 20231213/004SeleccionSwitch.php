<?php

//crear una variable llamada numero que puede valer( calcular aleatoriamente)
//1-100 (numero entero)
//imprimir 
//"alpe" si el numero es multiplo de 3
//"POO" si es multiplo de 5
//"alpePOO" si es multiplo de 3 y 5

$numero = rand(1, 100);

switch (true) {
    case $numero % 3 == 0 && $numero % 5 == 0:
        echo " El numero $numero  es alpePOO";
        break;
    case $numero % 5 == 0:
        echo "El numero $numero es POO";
        break;
    case $numero % 3 == 0:
        echo "El numero $numero es alpe";
        break;
    default:
        echo "El $numero no es multiplo ni de 3 ni de 5";
        break;
}

//realizarlo con if y elseif
// if ($numero % 3 == 0 && $numero % 5 == 0) {
//     echo " El numero $numero  es alpePOO";
// } elseif ($numero % 5 == 0) {
//     echo "El numero $numero es POO";
// } elseif ($numero % 3 == 0) {
//     echo "El numero $numero es alpe";
// } else {
//     echo "El $numero no es multiplo ni de 3 ni de 5";
// }


