<?php

//crear una variable llamada mes con un numero del 1 al 12
//debe mostrar el mes correspondiente
//realizarlo con switch


$mes = 10;
switch ($mes) {
    case 1:
        echo "mes de enero";
        break;
    case 2:
        echo "mes de febrero";
        break;
    case 3:
        echo "mes de marzo";
        break;
    case 4:
        echo "mes de abril";
        break;
    case 5:
        echo "mes de mayo";
        break;
    case 6:
        echo "mes de junio";
        break;
    case 7:
        echo "mes de julio";
        break;
    case 8:
        echo "mes de agosto";
        break;
    case 9:
        echo "mes de septiembre";
        break;
    case 10:
        echo "mes de octubre";
        break;
    case 11:
        echo "mes de noviembre";
        break;
    case 12:
        echo "mes de diciembre";
        break;
    default:
        echo "mes no valido";
        break;

    
    } 
    
    ?>