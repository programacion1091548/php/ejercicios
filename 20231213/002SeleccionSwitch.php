<?php

//crear una variable llamada color que puede valer
//r , v, a
//Imprimir los siguientes valores en función del color
//R=> rojo, v= verde, a= azul
//realizarlo con switch

$color = "A";
switch ($color) {
    case 'r':
    case 'R':    
        echo "rojo";
        break;
    case 'v':
    case 'V':
        echo "verde";
        break;
    case 'a':
    case 'A':
        echo "azul";
        break;
    default:
        echo "color no valido";
        break;

}

