<?php

//crear una variable llamada unidades que puede valer
//1-100
//imprimir el descuento que se aplica a la compra en función de las unidades solicitadas
//<10 => 1%
//>= 10 y 50 => 2%
//<50 => 10%
//Realizarlo con switch

$unidades = 15;
switch (true) {
    case ($unidades < 10):
        echo "descuento 1%";
        break;
    case ($unidades < 50):
        echo "descuento 2%";
        break;
    default:
        echo "descuento 10%";
        break;
}

//realizarlo con if y elseif
// if($unidades < 10){
//     echo "descuento 1%";
// }else if($unidades < 50){
//     echo "descuento 2%";
// }else{
//     echo "descuento 10%";
// }
