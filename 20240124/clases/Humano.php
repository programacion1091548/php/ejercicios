<?php

class Humano
{
    public  string $nombre;
    public  string $sexo;
    public string $fechaNacimiento;

public function presentarse()

{
    return "Hola soy " . $this->nombre . " y mi fecha de nacimiento es " . $this->fechaNacimiento;
}
public function __construct (string $nombre="", string $sexo="", string $fechaNacimiento="")
{
    $this->nombre = $nombre;
    $this->sexo = $sexo;
    $this->fechaNacimiento = $fechaNacimiento;
}
}