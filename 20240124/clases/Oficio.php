<?php
class Oficio
{
    public  string $nombre;
    public  float $salarioBase;
    public  int $horasSemanales;



public function __construct(string $nombre="", float $salarioBase=0, int $horasSemanales=0)
{
    $this->nombre = $nombre;
    $this->salarioBase = $salarioBase;
    $this->horasSemanales = $horasSemanales;

}
//calcular el sueldo por semana

public function calcular()
{
    return ($this->salarioBase * $this->horasSemanales);

}
}
