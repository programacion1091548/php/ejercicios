<?php
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';});

$persona1 = new Humano("pepe","M", "24/01/2010");
$persona2 = new Humano("juan","M", "11/03/1979");
$persona3 = new Humano("ana","F", "01/01/2001");

$oficio1 = new Oficio("programador",10,40);
$oficio2 = new Oficio("medico",15,50);
$oficio3 = new Oficio("enfermero",7.5,65);

$trabajan1 = new Trabajan($persona1, $oficio1);
$trabajan2 = new Trabajan($persona2, $oficio2);
$trabajan3 = new Trabajan($persona3, $oficio3);


echo "Persona 1: ". $trabajan1 -> persona -> presentarse();
echo "<br>Persona1: ". $trabajan1 -> oficio -> calcular();





