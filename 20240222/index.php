<?php

$parametros = require_once "parametros.php";

require_once "funciones.php";

// desactivar errores
controlErrores();

// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Mensaje" => "mensaje.php",
    "Salir" => ""
];

// preparo el menu
$menu = menu($elementosMenu);


// conexion a base de datos
$conexion = @new mysqli(
    $parametros['bd']["servidor"],
    $parametros['bd']["usuario"],
    $parametros['bd']["password"],
    $parametros['bd']["nombreBd"]

);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

$sql = "select * from mensajes";


// creo un objeto de tipo mysqli_result de la consulta
// y ademas compruebo si la consulta es correcta
if ($resultado = $conexion->query($sql)) {
    // crear un string con la tabla de todos los registros
    $salida = gridViewBotones($resultado, [
        "Editar" => "actualizar.php",
        "Eliminar" => "eliminar.php"
    ]);
} else {
    $salida = "Error al ejecutar la consulta: " . $conexion->error;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $parametros['aplicacion']["nombreAplicacion"] ?>
    </title>
</head>

<body>
    <h1>
        <?= $parametros['aplicacion']["nombreAplicacion"] ?>
    </h1>
    <div>
        <form method="post">
            <label for="usuario">Usuario</label>
            <input type="text" id="usuario" name="usuario"></input>
            <input type="submit" mame="usuario"></input>


    </div>
    </form>
    <?php if (isset($_POST["usuario"])) {
        gridViewBotones($resultado);
        ?>
        <div>
            <?= $menu ?>
            <?= $salida ?>
        </div>
        <?php
    }

    ?>
</body>

</html>